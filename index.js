/**
 * Partie index de notre site internet de chat en ligne pour jouer au Punto.
 *
 * @author Morgan LECLERCQ TP2A
 * @author Hugo BESSANT TP2A
 *
 * @version 1.0
 */
// Chargement des modules 
var express = require('express');
const { nombre_joueur, creerJoueur, tour_normal } = require('./punto');
var app = express();
var server = app.listen(8080, function() {
});

// Ecoute sur les websockets
var io = require('socket.io').listen(server);

// Configuration d'express pour utiliser le répertoire "public"
app.use(express.static('public'));
// set up to 
app.get('/', function(req, res) {  
    res.sendFile(__dirname + '/public/chat.html');
});

// déblocage requetes cross-origin
io.set('origins', '*:*');

/***************************************************************
 *           Gestion des clients et des connexions
 ***************************************************************/

var clients = {};       // { id -> socket, ... }


/***************************************************************
 *              Gestion des défis punto
 ***************************************************************/

var punto = require("./punto");

/**
 *  Supprime les infos associées à l'utilisateur passé en paramètre.
 *  @param  string  id  l'identifiant de l'utilisateur à effacer
 */
function supprimer(id) {
    delete clients[id];
    punto.supprimer(id);
}

// Quand un client se connecte, on le note dans la console
io.on('connection', function (socket) {
    
    // message de debug
    var currentID = null;
    /**
     *  Doit être la première action après la connexion.
     *  @param  id  string  l'identifiant saisi par le client
     */
    socket.on("login", function(id) {
        // si le pseudo est déjà utilisé, on lui envoie l'erreur
        if (clients[id]) {
            socket.emit("erreur-connexion", "Le pseudo est déjà pris.");
            return;
        }
        // sinon on récupère son ID
        currentID = id;
        // initialisation
        clients[currentID] = socket;
        punto.ajouter(currentID);
        // log
        console.log("Nouvel utilisateur : " + currentID);
        // scores 
        var scores = JSON.parse(punto.scoresJSON());
        // envoi d'un message de bienvenue à ce client
        socket.emit("bienvenue", scores);
        // envoi aux autres clients 
        socket.broadcast.emit("message", { from: null, to: null, text: currentID + " a rejoint la discussion", date: Date.now() });
        // envoi de la nouvelle liste à tous les clients connectés 
        socket.broadcast.emit("liste", scores);
    });
    
    /**
     *  Réception d'un message et transmission à tous.
     *  @param  msg     Object  le message à transférer à tous  
     */
    socket.on("message", function(msg) {
        console.log("Reçu message");   
        // si message privé, envoi seulement au destinataire
        if (msg.to != null) {
            if (clients[msg.to] !== undefined) {
                console.log(" --> message privé");
                clients[msg.to].emit("message", { from: currentID, to: msg.to, text: msg.text, date: Date.now() });
                if (currentID != msg.to) {
                    socket.emit("message", { from: currentID, to: msg.to, text: msg.text, date: Date.now() });
                }
            }
            else {
                socket.emit("message", { from: null, to: currentID, text: "Utilisateur " + msg.to + " inconnu", date: Date.now() });
            }
        }
        // sinon, envoi à tous les gens connectés
        else {
            console.log(" --> broadcast");
            io.sockets.emit("message", { from: currentID, to: null, text: msg.text, date: Date.now() });
        }
    });
    /**
     *  Réception d'un message et transmission à tous.
     *  @param  msg     Object  le message à transférer à tous  
     */
    socket.on("message_punto", function(msg,id) {
        console.log("Reçu message");   
        // si message privé, envoi seulement au destinataire
        if (msg.to != null) {
            if (clients[msg.to] !== undefined) {
                console.log(" --> message privé");
                clients[msg.to].emit("message_punto", { from: currentID, to: msg.to, text: msg.text, date: Date.now() },id);
                if (currentID != msg.to) {
                    socket.emit("message_punto", { from: currentID, to: msg.to, text: msg.text, date: Date.now() },id);
                }
            }
            else {
                socket.emit("message_punto", { from: null, to: currentID, text: "Utilisateur " + msg.to + " inconnu", date: Date.now() },id);
            }
        }
        // sinon, envoi à tous les gens connectés
        else {
            console.log(" --> broadcast");
            io.sockets.emit("message_punto", { from: currentID, to: null, text: msg.text, date: Date.now() },id);
        }
    });
    /**
     *  Réception d'une demande de défi punto
     */
    socket.on("punto", function(data,users) {
        if(data==="join"){
            var res = punto.joinPunto(currentID);
            if(res.status==-1){
                socket.emit("message", { from: 0, to: currentID, text: res.message, date: Date.now() });
            }else{
                //Creer une partie + 1 joueur + envoie parti aux autres joueurs
                var id=punto.creerPartie();
                punto.creerJoueur(currentID,id);
                //Envoie du defi punto aux autres joueurs
                for(let a=0;a<users.length;a++){
                    clients[users[a]].emit("punto",currentID,id);
                }
            }
        }else{

        }
       
    });
    /**
     * Réception d'un joueur qui rejoint une partie
     */
    socket.on("rejoindre_partie", function(user,id) {
        punto.creerJoueur(user,id);
        socket.broadcast.emit("message_punto", { from: null, to: null, text: currentID + " a rejoint la discussion", date: Date.now() });
    });
    /**
     * Réception d'un début de partie de punto
     */
    socket.on("jouer_punto",function(id,uti){
        //Si la partie s'est bien lancé
        if(punto.lancer_partie(id,uti)){
            //Envoie de la suppression du bouton lancer partie possible
            socket.emit("bouton_disparition",id);
            for(let a=0;a<punto.getPartie(id).nombre_joueur();a++){
                clients[punto.getPartie(id).joueurs[a].pseudo].emit("image_dos_transformation",id);
            }

            punto.tirage_carte_debut(id);
            //On effectue le premier tour pour le joueur 1 automatiquement
            punto.tour_initial(id);
            //Envoie de la grille de jeu actualisé aux joueurs
            for(let a=0;a<punto.getPartie(id).nombre_joueur();a++){
                clients[punto.getPartie(id).joueurs[a].pseudo].emit("affichage_initial",id,punto.getPartie(id).joueurs[a],punto.getPartie(id));
            }  
        }
    });
    /**
     * Réception d'un clic sur une case pour poser une carte
     */
    socket.on("indice_click",function(indice,id,jo,pseudo_clique){
        if(punto.tour_normal(id,indice,pseudo_clique)){
            for(let a=0;a<punto.getPartie(id).nombre_joueur();a++){
                if(!punto.getPartie(id).joueurs[a].ia){
                    //Envoie aux joueurs de l'actualisation de l'affichage de la grille
                    clients[punto.getPartie(id).joueurs[a].pseudo].emit("actualisation_affichage",id,punto.getPartie(id)); 
                    if(punto.getPartie(id).termine){
                        //Envoie aux joueurs la partie qui est terminé
                        clients[punto.getPartie(id).joueurs[a].pseudo].emit("partie_termine",id,punto.getPartie(id));
                    }
                }
            } 
        }
    });
    /**
     * Réception d'un joueur qui a quitté la partie en cours de jeu et remplacement par une IA faible
     */
    socket.on("remplacement_ia",function(user,id){
        let nb_ia=0;
        //Verification du nombre d'ia
        for(let i=0;i<punto.getPartie(id).nombre_joueur();i++){
            if(punto.getPartie(id).joueurs[i].ia){
                nb_ia++;
            }
        }
        //Si il reste des vrais joueurs
        if(nb_ia!=punto.getPartie(id).nombre_joueur()){
            for(let a=0;a<punto.getPartie(id).nombre_joueur();a++){
                if(punto.getPartie(id).joueurs[a].pseudo==user){
                    punto.getPartie(id).joueurs[a].ia=true;
                    let t=0;
                    let tour=true;
                    //On cherche la case sur laquelle peut jouer l'ia et on le fait jouer et actualisation affichage
                    while(tour){
                        tour=punto.tour_normal(id,t,user);
                        if(tour){
                            for(let b=0;b<punto.getPartie(id).nombre_joueur();b++){
                                if(!punto.getPartie(id).joueurs[b].ia){
                                    clients[punto.getPartie(id).joueurs[b].pseudo].emit("actualisation_affichage",id,punto.getPartie(id));
                                }
                            }  
                            break;
                        }
                        tour=true;
                        t++; 
                    }
                }
            }
        } 
    });
    /** 
     *  Gestion des déconnexions
     */
    // fermeture
    socket.on("logout", function() { 
        // si client était identifié (devrait toujours être le cas)
        if (currentID) {
            console.log("Sortie de l'utilisateur " + currentID);
            // envoi de l'information de déconnexion
            socket.broadcast.emit("message", 
                { from: null, to: null, text: currentID + " a quitté la discussion", date: Date.now() } );
            // suppression de l'entrée
            supprimer(currentID);
            // désinscription du client
            currentID = null;
             // envoi de la nouvelle liste pour mise à jour
            socket.broadcast.emit("liste", JSON.parse(punto.scoresJSON()));
        }
    });
    // déconnexion de la socket
    socket.on("disconnect", function(reason) { 
        // si client était identifié
        if (currentID) {
            // envoi de l'information de déconnexion
            socket.broadcast.emit("message", 
                { from: null, to: null, text: currentID + " vient de se déconnecter de l'application", date: Date.now() } );
            // suppression de l'entrée
            supprimer(currentID);
            // désinscription du client
            currentID = null;
            // envoi de la nouvelle liste pour mise à jour
            socket.broadcast.emit("liste", JSON.parse(punto.scoresJSON()));
        }
        console.log("Client déconnecté");
    });  
});