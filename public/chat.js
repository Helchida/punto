/**
 * Partie chat de notre site internet de chat en ligne pour jouer au Punto.
 *
 * @author Morgan LECLERCQ TP2A
 * @author Hugo BESSANT TP2A
 *
 * @version 1.0
 */
"use strict";


//const punto = require("../punto");

document.addEventListener("DOMContentLoaded", function(_e) {

    // socket ouverte vers le client
    var sock = io.connect();
    
    // utilisateur courant 
    var currentUser = null;
    
    // tous les utilisateurs (utile pour la complétion) 
    var users = [];
    
    // tous les caractères spéciaux (utile pour les remplacements et la complétion) 
    var specialChars = {
        ":paper:": "&#9995;",
        ":rock:": "&#9994;",
        ":scissors:": "&#x270C;",
        ":lizard:": "&#129295;",
        ":spock:": "&#128406;",
        ":smile:": "&#128578;",
        ":sad:": "&#128577;",
        ":laugh:": "&#128512;",
        ":wink:": "&#128521;",
        ":love:": "&#129392;",
        ":coeur:": "&#10084;",
        ":bisou:": "&#128536;",
        ":peur:": "&#128561;",
        ":whoa:": "&#128562;",
        ":mask:" : "&#128567;"
    }

    // réception du message de bienvenue
    sock.on("bienvenue", function(liste) {    
        if (currentUser) {
            // on change l'affichage du bouton
            document.getElementById("btnConnecter").value = "Se connecter";
            document.getElementById("btnConnecter").disabled = false;
            // on vide les zones de saisie
            document.querySelector("#content #main1").innerHTML = "";
            document.getElementById("monMessage").value = "";
            document.getElementById("login").innerHTML = currentUser;
            document.getElementById("radio2").checked = true;
            document.getElementById("monMessage").focus();
            afficherListe(liste);
        }
    });
    // réception d'une erreur de connexion
    sock.on("erreur-connexion", function(msg) {
        alert(msg);   
        document.getElementById("btnConnecter").value = "Se connecter";
        document.getElementById("btnConnecter").disabled = false;
    });
    
    // réception d'un message classique
    sock.on("message", function(msg) {
        if (currentUser) {
            afficherMessage(msg);
        }
    });

    // réception d'un message classique
    sock.on("message_punto", function(msg,id) {
        if (currentUser) {
            afficherMessagePunto(msg,id);
        }
    });

    // réception de la mise à jour d'une liste
    sock.on("liste", function(liste) {
        if (currentUser) {
            afficherListe(liste);
        }
    });

    // réception d'un défi
    sock.on("punto", function(adversaire,id) {
        if(adversaire!=currentUser){
          // petit effet spécial (class "buzz" à ajouter pour faire vibrer l'affichage)
            document.getElementById("content").classList.add("buzz");
            setTimeout(function() {
                document.getElementById("content").classList.remove("buzz");
            }, 500);
            var btnRepondre = document.createElement("button");
            btnRepondre.innerHTML = "Jouer";
            btnRepondre.addEventListener("click", function() { rejoindrePartie(id); });
            var p = document.createElement("p");
            p.classList.add("punto");
            p.innerHTML = (new Date()).toLocaleString('fr-FR', { timeZone: 'Europe/Paris' }).substr(13, 8) + 
                            " - [punto] : " + adversaire + " a lancé une partie de Punto ";     
            p.appendChild(btnRepondre);
            document.querySelector("#content #main1").appendChild(p);  
        }else{
            //creation de la partie HTML pour le jeu
            ajoutPartieHTML(id);
            //creation du bouton pour lancer la partie 
            var btnLancerPartie=document.createElement("input");
            btnLancerPartie.type="button";
            btnLancerPartie.value="Lancer Partie";
            btnLancerPartie.id=id+"btnLancerPartie";
            document.getElementById("main"+id).appendChild(btnLancerPartie); 
            btnLancerPartie.addEventListener("click",function(){lancerPartie(id,currentUser);});     
        }
    });
    
    // gestion des déconnexions de la socket --> retour à l'accueil
    sock.on("disconnect", function(reason) {
        currentUser = null;
        document.getElementById("radio1").checked = true;
        document.getElementById("pseudo").focus();
    });

    sock.on("bouton_disparition", function(id) {
        var name=id+"btnLancerPartie";
        document.getElementById(name).style.display="none";
    });

    sock.on("image_dos_transformation", function(id) {
        var name=id+"img";
    });


    sock.on("affichage_initial",function(id,jo,part){
        var div_ligne=document.createElement("div");
        div_ligne.className="div_tabl";
        div_ligne.id="div_ligne"+id;
        document.getElementById("main"+id).appendChild(div_ligne);
        for(let a=0;a<36;a++){
            var ch="";
            if(part.grille[a]!=undefined){
               if(part.grille[a].couleur=="red"){
                    ch="./images/R"+part.grille[a].valeur+".png";
                }else if(part.grille[a].couleur=="blue"){
                    ch="./images/B"+part.grille[a].valeur+".png";
                }else if(part.grille[a].couleur=="green"){
                    ch="./images/G"+part.grille[a].valeur+".png";
                }else{
                    ch="./images/Y"+part.grille[a].valeur+".png";
                } 
            }
            var div_petit=document.createElement("div");
            div_petit.id=id+"div_petit"+a;
            if(part.grille[a]!=undefined){
                div_petit.style.backgroundImage="url("+ch+")";
                div_petit.style.backgroundSize="100%";
            }
            div_petit.addEventListener("click",function(){
                sock.emit("indice_click",a,id,jo,currentUser);
            });
            document.getElementById("div_ligne"+id).appendChild(div_petit);
        }
        var imageJouer=document.createElement("img");
        if(jo.couleur=="red"){
            imageJouer.src="./images/R"+jo.cartes[jo.indice_carte].valeur+".png";
            imageJouer.id="imgRed"+id;
        }else if(jo.couleur=="blue"){
            imageJouer.src="./images/B"+jo.cartes[jo.indice_carte].valeur+".png";
            imageJouer.id="imgBlue"+id;
        }else if(jo.couleur=="green"){
            imageJouer.src="./images/G"+jo.cartes[jo.indice_carte].valeur+".png";
            imageJouer.id="imgGreen"+id;
        }else{
            imageJouer.src="./images/Y"+jo.cartes[jo.indice_carte].valeur+".png";
            imageJouer.id="imgYellow"+id;
        }
        imageJouer.style.maxWidth="7%";
        imageJouer.style.position="relative";
        imageJouer.style.left="880px";
        imageJouer.style.bottom="300px";
        document.getElementById("main"+id).appendChild(imageJouer);
    });

    sock.on("actualisation_affichage", function(id,part) {
        let k=0;
        for(let i=0;i<part.joueurs.length;i++){
            if(part.joueurs[i]!=undefined){
                if(part.joueurs[i].peut_jouer!=true){
                    k++
                }else{
                    break;
                }
            }
        }
        //ACTUALISATION GRILLE
        for(let a=0;a<36;a++){
            var name=id+"div_petit"+a;
            var fils=document.getElementById(name);
            document.getElementById("div_ligne"+id).removeChild(fils);
        }
        var fils_2=document.getElementById("div_ligne"+id);
        document.getElementById("main"+id).removeChild(fils_2);
        var div_ligne=document.createElement("div");
        div_ligne.className="div_tabl";
        div_ligne.id="div_ligne"+id;
        document.getElementById("main"+id).appendChild(div_ligne);
        for(let a=0;a<36;a++){
            var ch="";
            if(part.grille[a]!=undefined){
               if(part.grille[a].couleur=="red"){
                    ch="./images/R"+part.grille[a].valeur+".png";
                }else if(part.grille[a].couleur=="blue"){
                    ch="./images/B"+part.grille[a].valeur+".png";
                }else if(part.grille[a].couleur=="green"){
                    ch="./images/G"+part.grille[a].valeur+".png";
                }else{
                    ch="./images/Y"+part.grille[a].valeur+".png";
                } 
            }
            var div_petit=document.createElement("div");
            div_petit.id=id+"div_petit"+a;
            if(part.grille[a]!=undefined){
                div_petit.style.backgroundImage="url("+ch+")";
                div_petit.style.backgroundSize="100%";
            }
            div_petit.addEventListener("click",function(){
                sock.emit("indice_click",a,id,part.joueurs[k],currentUser);
            });
            document.getElementById("div_ligne"+id).appendChild(div_petit);
        }
        //ACTUALISATION CARTE DROITE
        var col="";
        let a=0;
        while(part.joueurs[a].pseudo!=currentUser){
            a++;
        }
        //Pour la couleur de la carte
        col=part.joueurs[a].couleur;
        if(col=="red"){
        var fils_3=document.getElementById("imgRed"+id);
        document.getElementById("main"+id).removeChild(fils_3);
        }else if(col=="blue"){
            var fils_3=document.getElementById("imgBlue"+id);
            document.getElementById("main"+id).removeChild(fils_3);
        }else if(col=="green"){
            var fils_3=document.getElementById("imgGreen"+id);
            document.getElementById("main"+id).removeChild(fils_3);
        }else{
            var fils_3=document.getElementById("imgYellow"+id);
            document.getElementById("main"+id).removeChild(fils_3);
        }        
        var imageJouer=document.createElement("img");
        //En fonction de la couleur la carte se nome (R9 pour une carte rouge 9 valeur)
        if(col=="red"){
            imageJouer.src="./images/R"+part.joueurs[a].cartes[part.joueurs[a].indice_carte].valeur+".png";
            imageJouer.id="imgRed"+id;
        }else if(col=="blue"){
            imageJouer.src="./images/B"+part.joueurs[a].cartes[part.joueurs[a].indice_carte].valeur+".png";
            imageJouer.id="imgBlue"+id;
        }else if(col=="green"){
            imageJouer.src="./images/G"+part.joueurs[a].cartes[part.joueurs[a].indice_carte].valeur+".png";
            imageJouer.id="imgGreen"+id;
        }else{
            imageJouer.src="./images/Y"+part.joueurs[a].cartes[part.joueurs[a].indice_carte].valeur+".png";
            imageJouer.id="imgYellow"+id;
        }
        //MOdification du css
        imageJouer.style.maxWidth="7%";
        imageJouer.style.position="relative";
        imageJouer.style.left="880px";
        imageJouer.style.bottom="300px";
        document.getElementById("main"+id).appendChild(imageJouer);        
    });
   
    //Instruction quand la partie est finie 
    sock.on("partie_termine", function(id,part) {
        var btnRetourChat=document.createElement("input");
        btnRetourChat.type="button";
        btnRetourChat.value="Retour Chat";
        btnRetourChat.id=id+"btnRetourChat";
        btnRetourChat.style.display="unset";
        btnRetourChat.style.marginLeft="unset";
        btnRetourChat.style.marginRight="unset";
        btnRetourChat.style.top="unset";
        btnRetourChat.style.bottom="270px";
        btnRetourChat.style.left="780px";
        document.getElementById("main"+id).appendChild(btnRetourChat); 
        btnRetourChat.addEventListener("click",function(){retourChat();});
        var textGagnant=document.createElement("div");
        var pseudoGagnant="";
        for(let a=0;a<part.joueurs.length;a++){
            if(part.joueurs[a].a_gagne){
                pseudoGagnant+=part.joueurs[a].pseudo;
            }
        }
        //Affichage du texte du gagnant
        textGagnant.id="div"+pseudoGagnant;
        textGagnant.innerHTML=pseudoGagnant;
        textGagnant.style.position="relative";
        textGagnant.style.bottom="450px";
        textGagnant.style.left="880px";
        document.getElementById("main"+id).appendChild(textGagnant); 
        var text=document.createElement("div");
        text.id="divAgagne";
        text.innerHTML=" a gagné !";
        text.style.position="relative";
        text.style.bottom="470px";
        text.style.left="870px";
        document.getElementById("main"+id).appendChild(text);  
    });

    function ajoutPartieHTML(id){
        //AJout un par un des elements de l'html necessaire pour la affichage de la partie
        var inputGame=document.createElement("input");
        inputGame.type="radio";
        inputGame.name="btnScreen";
        inputGame.id="radio"+id;
        document.body.appendChild(inputGame);
        var divId=document.createElement("div");
        divId.id="content"+id;
        divId.className="divclass";
        document.body.appendChild(divId); 
        var h2=document.createElement("h2");
        h2.innerHTML="Punto - Joueur : <span id=\"login"+id+"\"></span>";
        let name = "content"+id;
        document.getElementById(name).appendChild(h2);
        var h3=document.createElement("h3");
        h3.innerHTML="Chat";
        document.getElementById(name).appendChild(h3);
        var aside=document.createElement("aside");
        aside.id="aside"+id;
        aside.className="aside";
        document.getElementById(name).appendChild(aside);
        var main=document.createElement("main");
        main.id="main"+id;
        main.className="main";
        document.getElementById(name).appendChild(main);
        var footer=document.createElement("footer");
        footer.id="footer"+id;
        let nameFooter="footer"+id;
        document.getElementById(name).appendChild(footer);
        var input1=document.createElement("input");
        input1.type="text";
        input1.id="monMessage"+id;
        document.getElementById(nameFooter).appendChild(input1);
        var input2=document.createElement("input");
        input2.type="button";
        input2.value="Envoyer";
        input2.id="btnEnvoyer"+id;
        document.getElementById(nameFooter).appendChild(input2);
        document.getElementById("btnEnvoyer"+id).addEventListener("click",function(){envoyerMessagePunto(id);});
        var input3=document.createElement("input");
        input3.type="button";
        input3.value="Déconnexion";
        input3.id="btnQuitter"+id;
        document.getElementById(nameFooter).appendChild(input3);
        let nameLogin="login"+id;
        document.getElementById(nameLogin).innerHTML = currentUser;
        let nameRadio="radio"+id;
        document.getElementById(nameRadio).checked = true;
        document.getElementById("pseudo").focus();
        document.getElementById("btnQuitter"+id).addEventListener("click", function() {quitter2(id);});
        var imageDos=document.createElement("img");
        imageDos.src="./images/Dos.png";
        imageDos.id=id+"img";
    }

    //Fonction de retour au chat
    function retourChat(){
        document.getElementById("login").innerHTML = currentUser;
        document.getElementById("radio2").checked = true;
        document.getElementById("pseudo").focus();
    }
    
    /** 
     *  Connexion de l'utilisateur au chat.
     */
    function connect() {
        // recupération du pseudo
        var user = document.getElementById("pseudo").value.trim();
        if (! user) return;
        currentUser = user; 
        // ouverture de la connexion
        sock.emit("login", user);
        document.getElementById("btnConnecter").value = "En attente...";
        document.getElementById("btnConnecter").disabled = true;
    }

    //Fonction pour afficher un message lors d'une partie de jeu
    function afficherMessagePunto(msg,id) {
        // si réception du message alors que l'on est déconnecté du service
        if (!currentUser) return;   
        // affichage des nouveaux messages 
        var bcMessages = document.getElementById("aside"+id);
        // identification de la classe à appliquer pour l'affichage
        var classe = "";        
        // cas des messages privés 
        if (msg.from != null && msg.to != null && msg.from != 0) {
            classe = "mp";  
            if (msg.from == currentUser) {
                msg.from += " [privé @" + msg.to + "]";   
            }
            else {
                msg.from += " [privé]"
            }
        }
        // cas des messages ayant une origine spécifique (soi-même ou le serveur)
        if (msg.from == currentUser) {
            classe = "moi";   
        }
        else if (msg.from == null) {
            classe = "system";
            msg.from = "[admin]";
        }
        else if (msg.from === 0) {
            classe = "punto";
            msg.from = "[punto]";
        }
        // affichage de la date format ISO pour avoir les HH:MM:SS finales qu'on extrait ensuite
        var date = new Date(msg.date);
        date = date.toLocaleString('fr-FR', { timeZone: 'Europe/Paris' }).substr(13, 8);
        // remplacement des caractères spéciaux par des émoji
        msg.text = traiterTexte(msg.text);
        // affichage du message
        bcMessages.innerHTML += "<p class='" + classe + "'>" + date + " - " + msg.from + " : " + msg.text + "</p>"; 
    };

    /** 
     *  Affichage des messages 
     *  @param  object  msg    { from: auteur, text: texte du message, date: horodatage (ms) }
     */
    function afficherMessage(msg) {
        // si réception du message alors que l'on est déconnecté du service
        if (!currentUser) return;   
        // affichage des nouveaux messages 
        var bcMessages = document.querySelector("#content #main1");
        // identification de la classe à appliquer pour l'affichage
        var classe = "";        
        // cas des messages privés 
        if (msg.from != null && msg.to != null && msg.from != 0) {
            classe = "mp";  
            if (msg.from == currentUser) {
                msg.from += " [privé @" + msg.to + "]";   
            }
            else {
                msg.from += " [privé]"
            }
        }
        // cas des messages ayant une origine spécifique (soi-même ou le serveur)
        if (msg.from == currentUser) {
            classe = "moi";   
        }
        else if (msg.from == null) {
            classe = "system";
            msg.from = "[admin]";
        }
        else if (msg.from === 0) {
            classe = "punto";
            msg.from = "[punto]";
        }
        // affichage de la date format ISO pour avoir les HH:MM:SS finales qu'on extrait ensuite
        var date = new Date(msg.date);
        date = date.toLocaleString('fr-FR', { timeZone: 'Europe/Paris' }).substr(13, 8);
        // remplacement des caractères spéciaux par des émoji
        msg.text = traiterTexte(msg.text);
        // affichage du message
        bcMessages.innerHTML += "<p class='" + classe + "'>" + date + " - " + msg.from + " : " + msg.text + "</p>"; 
        // scroll pour que le texte ajouté soit visible à l'écran
        document.querySelector("main > p:last-child").scrollIntoView();
    };
    
    // traitement des emojis
    function traiterTexte(txt) {
        // remplacement de quelques smileys par les :commandes: correspondantes
        txt = txt.replace(/:[-]?\)/g,':smile:');
        txt = txt.replace(/:[-]?[Dd]/g,':laugh:');
        txt = txt.replace(/;[-]?\)/g,':wink:');
        txt = txt.replace(/:[-]?[oO]/g,':whoa:');
        txt = txt.replace(/:[-]?\*/g,':bisou:');
        txt = txt.replace(/<3/g,':coeur:');
        // remplacement des :commandes: par leur caractère spécial associé 
        for (let sp in specialChars) {
            txt = txt.replace(new RegExp(sp, "gi"), specialChars[sp]);   
        }
        return txt;   
    }
    
    /**
     *  Affichage de la liste de joueurs.
     */
    function afficherListe(newList) {
        // liste des joueurs
        users = Object.keys(newList);
        // tri des joueurs en fonction de leur classement
        users.sort(function(u1, u2) { return newList[u2] - newList[u1]; });
        // affichage en utilisant l'attribut personnalisé data-score
        document.querySelector("#content aside").innerHTML = 
        users.map(u => "<p data-score='" + newList[u] + "'>" + u + "</p>").join("");
    }

    /**
     *  Envoi d'un message : 
     *      - Récupération du message dans la zone de saisie.
     *      - Identification des cas spéciaux : @pseudo ... ou /punto @pseudo :choix:
     *      - Envoi au serveur via la socket
     */ 
    function envoyer() {
        
        var msg = document.getElementById("monMessage").value.trim();
        if (!msg) return;   

        // Cas des messages privés
        var to = null;
        if (msg.startsWith("@")) {
            var i = msg.indexOf(" ");
            to = msg.substring(1, i);
            msg = msg.substring(i);
        }
        
        // Cas d'un punto
        if (msg.startsWith("/punto")) {
            var args = msg.split(" ").filter(e => e.length > 0);
            if (args.length < 2) {
                afficherMessage({from: null, text: "Usage : /punto join", date: Date.now() });
                return;
            }
            if (args[1] !== "join" && args[1]!=="leave") {
                afficherMessage({from: null, text: "Le second argument doit être join ou leave", date: Date.now() });
                return;
            }
            
            sock.emit("punto", args[1],users);
        }else {
            // envoi
            sock.emit("message", { to: to, text: msg });
        }
        // enregistrement de la commande dans l'historique
        historique.ajouter();
        // effacement de la zone de saisie
        document.getElementById("monMessage").value = "";
    }

    //Fonction d'envoie d'un message lors d'une partie
    function envoyerMessagePunto(id) {
        var msg = document.getElementById("monMessage"+id).value.trim();
        if (!msg) return;   
        // Cas des messages privés
        var to = null;
        if (msg.startsWith("@")) {
            var i = msg.indexOf(" ");
            to = msg.substring(1, i);
            msg = msg.substring(i);
        }
        // Cas d'un punto
        if (msg.startsWith("/punto")) {
            var args = msg.split(" ").filter(e => e.length > 0);
            if (args.length < 2) {
                afficherMessagePunto({from: null, text: "Usage : /punto join", date: Date.now() },id);
                return;
            }
            if (args[1] !== "join" && args[1]!=="leave") {
                afficherMessagePunto({from: null, text: "Le second argument doit être join ou leave", date: Date.now() },id);
                return;
            }
            sock.emit("punto", args[1],users);
        }else {
            // envoi
            sock.emit("message_punto", { to: to, text: msg },id);
        }
        // enregistrement de la commande dans l'historique
        historique.ajouter();
        // effacement de la zone de saisie
        document.getElementById("monMessage"+id).value = "";
    }
    
    //Code a executer lorsque l'on rejoint une partie
    function rejoindrePartie(id) {
        ajoutPartieHTML(id);
        document.getElementById("radio"+id).checked = true;
        document.getElementById("pseudo").focus();
        sock.emit("rejoindre_partie",currentUser,id);
    }

    function lancerPartie(id,ut) {
        sock.emit("jouer_punto",id,ut);
    }
    
    /**
     *  Quitter le chat et revenir à la page d'accueil.
     */
    function quitter() { 
        if (confirm("Quitter le chat ?")) {
            currentUser = null;
            sock.emit("logout");
            document.getElementById("radio1").checked = true;
        }
    };    
    
    //Pour quitter dans une partie de Punto
    function quitter2(id) { 
        if (confirm("Quitter l'application ?")) {
            sock.emit("remplacement_ia",currentUser,id);
            currentUser = null;
            sock.emit("logout");
            document.getElementById("radio1").checked = true;
        }
    };  
    // Objet singleton gérant l'auto-complétion
    var completion = {
        // le texte de base que l'on souhaite compléter
        text: null,
        // l'indice de la proposition courant de complétion
        index: -1,
        // la liste des propositions de complétion
        props: [],
        // remise à zéro
        reset: function() {
            this.text = null;
            this.index = -1;
        },
        // calcul de la proposition suivante et affichage à l'emplacement choisi
        next: function() {
            if (this.text === null) {
                this.text = document.getElementById("monMessage").value;
                this.props = this.calculePropositions();
                this.text = this.text.substring(0, this.text.lastIndexOf(":"));
                this.index = -1;
                if (this.props.length == 0) {
                    this.text = null;
                    return;
                }
            }
            this.index = (this.index + 1) % this.props.length;
            document.getElementById("monMessage").value = this.text + this.props[this.index];   
        },
        // calcul des propositions de compétion
        calculePropositions: function() {
            var i = this.text.lastIndexOf(":");
            if (i >= 0) { 
                var prefixe = this.text.substr(i);
                return Object.keys(specialChars).filter(e => e.startsWith(prefixe));
            }
            return [];
        }
    };
    
    // Objet singleton gérant l'historique des commandes saisies
    var historique = {
        // contenu de l'historique
        content: [],
        // indice courant lors du parcours
        index: -1,
        // sauvegarde de la saisie en cour
        currentInput: "",
        
         precedent: function() {
            if (this.index >= this.content.length - 1) {
                return;
            }
            // sauvegarde de la saisie en cours
            if (this.index == -1) {
                this.currentInput = document.getElementById("monMessage").value;
            }
            this.index++;
            document.getElementById("monMessage").value = this.content[this.index];
            completion.reset();
        },
        suivant: function() {
            if (this.index == -1){
                return;
            }
            this.index--;
            if (this.index == -1) {
                document.getElementById("monMessage").value = this.currentInput;
            }
            else{
                document.getElementById("monMessage").value = this.content[this.index];
            }
            completion.reset();
        },
        ajouter: function() {
            this.content.splice(0, 0, document.getElementById("monMessage").value);
            this.index = -1;
        }
    }
    
    /** 
     *  Mapping des boutons de l'interface avec des fonctions du client.
     */
    document.getElementById("btnConnecter").addEventListener("click", connect);
    document.getElementById("btnQuitter").addEventListener("click", quitter);
    document.getElementById("btnEnvoyer").addEventListener("click", envoyer);

    /**
     *  Ecouteurs clavier
     */
    document.getElementById("pseudo").addEventListener("keydown", function(e) {
        if (e.keyCode == 13) // touche entrée
            connect();
    });
    document.getElementById("monMessage").addEventListener("keydown", function(e) {
        switch (e.keyCode) {
            case 9 : // tabulation
                e.preventDefault();     // empêche de perdre le focus
                completion.next();
                break;
            case 38 :   // fleche haut
                e.preventDefault();     // empêche de faire revenir le curseur au début du texte
                historique.precedent();
                break;
            case 40 :   // fleche bas
                e.preventDefault();     // par principe
                historique.suivant();
                break;
            case 13 :   // touche entrée
                envoyer();
            default: 
                completion.reset();
        }
    });

    document.querySelector("#content aside").addEventListener("dblclick", function(e) {
        if (e.target.tagName == "P") {
            initierDefi(e.target.innerHTML);
        }
    });
    
    // Au démarrage : force l'affichage de l'écran de connexion
    document.getElementById("radio1").checked = true;
    document.getElementById("pseudo").focus();  
}); 