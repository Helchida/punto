/**
 * Partie du jeu de punto de notre site internet de chat en ligne pour jouer au Punto.
 *
 * @author Morgan LECLERCQ TP2A
 * @author Hugo BESSANT TP2A
 *
 * @version 1.0
 */
  // défis en cours
var defis = {};         // { id -> { adversaire1 => choix1, adversaire2 => choix2 }, ... }
// scores des participants 
var scores = {};        // { id -> number, ... }


var beats = { 
    "rock": { "scissors": "crushes", "lizard": "crushes" }, 
    "lizard": { "spock": "poisons", "paper": "eats" },
    "scissors": { "lizard": "decapitate", "paper" : "cut" },
    "spock": { "scissors" : "smashes", "rock": "vaporizes" },
    "paper": { "rock": "covers", "spock": "disproves" }
};

/**
 * Classe Joueur qui définit un joueur 
 *      string pseudo
 *      string couleur
 *      array de carte
 *      int indice de carte a jouer
 *      boolean peut_jouer
 *      boolean a_gagne
 *      boolean est il une ia
 *      
 */
class Joueur {
    constructor(pseudo,couleur){
        this.pseudo=pseudo;
        this.couleur=couleur;
        this.cartes=new Array(18);
        this.indice_carte=-1;
        this.peut_jouer=false;
        this.a_gagne=false;
        this.ia=false;
        this.score=0;
    }

    /**
     * Fonction qui initialise les cartes du joueur
     */
    initialiserMain(){
        for(let i=0;i<this.cartes.length;i++){
            this.cartes[i]=new Carte(this.couleur,(i%9)+1);
        }
    }
}
/**
 * Classe carte qui definit une carte
 *      string couleur
 *      int valeur
 *      boolean est_dispo
 *      int x
 *      int y
 */
class Carte {
    constructor(couleur,valeur){
        this.couleur=couleur;
        this.valeur=valeur;
        this.est_dispo=true;
        this.x=-1;
        this.y=-1;
    }
    /**
     * 
     * Fonction qui dit si une carte est supérieure à une autre 
     */
    est_superieur(carte2){
        if(this.valeur>carte2.valeur){
            return true;
        }
        return false;
    }
}
/**
 * Classe Partie qui définit une partie
 *      int id de la partie
 *      array de joueurs
 *      array de carte
 *      boolean a_commence
 *      boolean est_termine
 */
class Partie {
    constructor(id){
        this.id=id;
        this.joueurs=new Array();
        this.grille=new Array(36);
        this.a_commence=false;
        this.termine=false;
    }
    /**
     * 
     * Fonction qui ajoute un joueur à une partie 
     */
    ajouterJoueur(j){
        this.joueurs.push(j);
    }
    /**
     * Fonction qui compte le nombre de joueurs de la partie
     */
    nombre_joueur(){
        return this.joueurs.length;
    }
    /**
     * Fonction qui verifie si le pseudo est présent dans la partie
     */
    verif_nom(pseudo){
        for(let a=0;a<this.joueurs.length;a++){
            if(pseudo==this.joueurs[a].pseudo){
                return true;
            }
        }
        return false;
    }
}
 
//Objet partie qui contient toutes les parties
var parties={};
//Variable qui permet de donner un id à une nouvelle partie
var prochain_id=3;

/**
 * Fonctio qui retourne une partie 
 */
function getPartie(id){
    return parties[id];
}
/**
 * Fonction qui creer une partie
 */
function creerPartie(){
    parties[prochain_id]=new Partie(prochain_id);
    console.log("Creer partie : "+parties[prochain_id].id);
    prochain_id++;
    return prochain_id-1;
}
/**
 * Fonction qui creer un joueur et le met dans une partie
 */
function creerJoueur(pseudo,id){
    if(parties[id]){
        if(parties[id].a_commence){
            return false;
        }
        var couleur; 
        //Attribution couleur au joueur
        if(parties[id].nombre_joueur()==0){ 
            couleur="red";
        }else if(parties[id].nombre_joueur()==1){
            couleur="blue";
        }
        else if(parties[id].nombre_joueur()==2){
            couleur="yellow";
        }
        else if(parties[id].nombre_joueur()==3){
            couleur="green";
        }else{
            return false;
        }
        if(parties[id].verif_nom(pseudo)){
            return false;
        }
        var j=new Joueur(pseudo,couleur);
        j.initialiserMain();
        console.log("Info joueur : "+j.cartes[0].est_dispo);
        parties[id].joueurs.push(j);
        console.log("Creer joueur : "+parties[id].joueurs[parties[id].joueurs.length-1].cartes[0].est_dispo);
        return true;
    }
    return false;
}
/**
 * Fonction qui verifie si on peut lancer une partie
 */
function lancer_partie(id,user){
    if(parties[id]){
        if(user!=parties[id].joueurs[0].pseudo){
            return false;
        }
        console.log(parties[id].nombre_joueur());
        if(parties[id].nombre_joueur()<2){
            return false;
        }
        parties[id].a_commence=true;
        return true;
    }
    return false;
}
/**
 * Fonction qui tire une carte au tour initial
 */
function tirage_carte_debut(id){
    if(parties[id]){
        for(let a=0;a<parties[id].joueurs.length;a++){
            let i;
            //Tirage tant que la carte n'est pas dispo
            do{
                i=Math.floor(Math.random()*(17-0+1)); 
            }while(!parties[id].joueurs[a].cartes[i].est_dispo);
            parties[id].joueurs[a].indice_carte=i;
        }
        return true;
    }
    return false;
}
/**
 * Fonction qui tire une carte dans un tour normal
 */
function tirage_carte(jo,id){
    if(parties[id]){
        let i;
        do{
           i=Math.floor(Math.random()*(17-0+1)); 
        }while(!jo.cartes[i].est_dispo);
        jo.indice_carte=i;
        return true;
    }
    return false;
}
/**
 * Fonction qui effectue le tour initial et dit si il est bien effectué
 */
function tour_initial(id){
    if(parties[id]){
        var indice=parties[id].joueurs[0].indice_carte;
        parties[id].grille[14]=parties[id].joueurs[0].cartes[indice];
        parties[id].joueurs[0].cartes[indice].est_dispo=false;
        tirage_carte(parties[id].joueurs[0],id);
        parties[id].joueurs[1].peut_jouer=true;
        return true;
    }
    return false;
}
/**
 * Fonction qui effectue un tout et dit si il est bien effectué
 */
function tour_normal(id,i,pseudo){
    if(parties[id] && !parties[id].termine){
        let k=0;
        while(!parties[id].joueurs[k].peut_jouer){
            k++;
        }
        //Verif que c'est bien à ce joueur de jouer
        if(parties[id].joueurs[k].peut_jouer && parties[id].joueurs[k].pseudo==pseudo){
            //Verif que la carte à poser est bien à coté d'une autre carte
            if(aCarteAdjacente(i,id) || parties[id].grille[i]!=undefined){
                if(parties[id].grille[i]==undefined){
                    parties[id].grille[i]=parties[id].joueurs[k].cartes[parties[id].joueurs[k].indice_carte];
                    parties[id].joueurs[k].cartes[parties[id].joueurs[k].indice_carte].est_dispo=false;
                    //Verif si le joueur a gagne
                    if(a_gagne(id,i,parties[id].joueurs[k])){
                        parties[id].termine=true;
                        parties[id].joueurs[k].peut_jouer=false;
                        parties[id].joueurs[k].a_gagne=true;
                        return true;
                    }
                    parties[id].joueurs[k].peut_jouer=false;
                    parties[id].joueurs[(k+1)%parties[id].nombre_joueur()].peut_jouer=true;
                    tirage_carte(parties[id].joueurs[k],id);
                    if(parties[id].joueurs[(k+1)%parties[id].nombre_joueur()].ia){
                        let t=0;
                        while(!tour_normal(id,t,parties[id].joueurs[(k+1)%parties[id].nombre_joueur()].pseudo) && t!=36){
                            t++;
                        }
                    }
                    return true;
                }else if(parties[id].grille[i].valeur<parties[id].joueurs[k].cartes[parties[id].joueurs[k].indice_carte].valeur){
                    parties[id].grille[i]=parties[id].joueurs[k].cartes[parties[id].joueurs[k].indice_carte];
                    parties[id].joueurs[k].cartes[parties[id].joueurs[k].indice_carte].est_dispo=false;
                    //Verif si le joueur a gagne
                    if(a_gagne(id,i,parties[id].joueurs[k])){
                        parties[id].termine=true;
                        parties[id].joueurs[k].peut_jouer=false;
                        parties[id].joueurs[k].a_gagne=true;
                        return true;
                    }
                    parties[id].joueurs[k].peut_jouer=false;
                    parties[id].joueurs[(k+1)%parties[id].nombre_joueur()].peut_jouer=true;
                    tirage_carte(parties[id].joueurs[k],id);
                    if(parties[id].joueurs[(k+1)%parties[id].nombre_joueur()].ia){
                        let t=0;
                        while(!tour_normal(id,t,parties[id].joueurs[(k+1)%parties[id].nombre_joueur()].pseudo) && t!=36){
                            t++;
                        }
                    }
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }else{
        return false;
    }
}
function aCarteAdjacente(i,id){
    if(parties[id]){
        //CAS CENTRAL AVEC 8 CARTES AUTOUR
        if((i>=7 && i<=10) || (i>=13 && i<=16) || (i>=19 && i<=22) || (i>=25 && i<=28)){
            for(let j=i-7;j<=i-5;j++){
                if(parties[id].grille[j]!=undefined){
                    return true;
                }
            }
            for(let j=i+5;j<=i+7;j++){
                if(parties[id].grille[j]!=undefined){
                    return true;
                }
            }
            if(parties[id].grille[i-1]!=undefined || parties[id].grille[i+1]!=undefined){
                return true;    
            
            }
            return false;

        }else if(i>=1 && i<=4){
            //CAS BORDURE SUP HORS COIN
            for(let j=i+5;j<=i+7;j++){
                if(parties[id].grille[j]!=undefined){
                    return true;
                }
            }
            if(parties[id].grille[i-1]!=undefined || parties[id].grille[i+1]!=undefined){
                return true;
            }
            console.log("TEST 6");
            return false;
        }else if(i>=31 && i<=34){
            //CAS BORDURE INF HORS COIN
            for(let j=i-7;j<=i-5;j++){
                if(parties[id].grille[j]!=undefined){
                    return true;
                }
            }
            if(parties[id].grille[i-1]!=undefined || parties[id].grille[i+1]!=undefined){
                return true;
            }
            return false;
        }else if(i==6 || i==12 || i==18 || i==24){
            //CAS BORDURE GAUCHE HORS COIN
            if(parties[id].grille[i-6]!=undefined || parties[id].grille[i-5]!=undefined || parties[id].grille[i+1]!=undefined || parties[id].grille[i+6]!=undefined || parties[id].grille[i+7]!=undefined){
                return true;
            }
            return false;
        }else if(i==11 || i==17 || i==23 || i==29){
            //CAS BORDURE DROITE HORS COIN
            if(parties[id].grille[i-6]!=undefined || parties[id].grille[i-7]!=undefined || parties[id].grille[i-1]!=undefined || parties[id].grille[i+5]!=undefined || parties[id].grille[i+6]!=undefined){
                return true;
            }
            return false;
        }else if(i==0){
            //CAS COIN SUP GAUCHE
            if(parties[id].grille[i+1]!=undefined || parties[id].grille[i+6]!=undefined || parties[id].grille[i+7]!=undefined){
                return true;
            }
            return false;
        }else if(i==5){
            //CAS COIN SUP DROIT
            if(parties[id].grille[i-1]!=undefined || parties[id].grille[i+6]!=undefined || parties[id].grille[i+5]!=undefined){
                return true;
            }
            return false;
        }else if(i==30){
            //CAS COIN INF GAUCHE
            if(parties[id].grille[i+1]!=undefined || parties[id].grille[i-6]!=undefined || parties[id].grille[i-5]!=undefined){
                return true;
            }
            return false;
        }else if(i==35){
            //CAS COIN INF DROIT
            if(parties[id].grille[i-6]!=undefined || parties[id].grille[i-7]!=undefined || parties[id].grille[i-1]!=undefined){
                return true;
            }
            return false;
        }
    }
    return false;
}
/**
 * Fonction qui verifie si on peut aller en haut
 */
function aller_haut(i){
    if(i>5){
        return true;
    }
    return false;
}
/**
 * Fonction qui verifie si on peut aller en bas
 */
function aller_bas(i){
    if(i<30){
        return true;
    }
    return false;
}
/**
 * Fonction qui verifie si on peut aller à droite
 */
function aller_droite(i){
    if(i!=5 && i!=11 && i!=17 && i!=23 && i!=29 && i!=35){
        return true;
    }
    return false;
}
/**
 * Fonction qui verifie si on peut aller à gauche
 */
function aller_gauche(i){
    if(i!=0 && i!=6 && i!=12 && i!=18 && i!=24 && i!=30){
        return true;
    }
    return false;
}
/**
 * Fonction qui verifie si un joueur a gagne lors de son tour
 */
function a_gagne(id,i,jo){
    if(parties[id]){
        
        var cpt=1;
        //TEST VICTOIRE VERTICAL
        var j=i;
        var keep=true;
        while(aller_haut(j) && keep && parties[id].grille[j-6]!=undefined){
            j=j-6;
            if(parties[id].grille[j].couleur==jo.couleur){
                cpt++;
                if(cpt==4){
                    return true;
                }
            }else{
                keep=false;
            }
        }
        j=i;
        keep=true;
        while(aller_bas(j) && keep && parties[id].grille[j+6]!=undefined){
            j=j+6
            if(parties[id].grille[j].couleur==jo.couleur){
                cpt++;
                if(cpt==4){
                    return true;
                }
            }else{
                keep=false;
            }
        }
        //TEST VICTOIRE HORIZONTAL
        cpt=1;
        j=i;
        keep=true;
        while(aller_droite(j) && keep && parties[id].grille[j+1]!=undefined){
            j++;
            if(parties[id].grille[j].couleur==jo.couleur){
                cpt++;
                if(cpt==4){
                    return true;
                }
            }else{
                keep=false;
            }
        }
        j=i;
        keep=true;
        while(aller_gauche(j) && keep && parties[id].grille[j-1]!=undefined){
            j--;
            if(parties[id].grille[j].couleur==jo.couleur){
                cpt++;
                if(cpt==4){
                    return true;
                }
            }else{
                keep=false;
            }
        }
        //TEST VICTOIRE DIAGONAL 1 (HAUT GAUCHE BAS DROIT)
        cpt=1;
        j=i;
        keep=true;
        while((aller_gauche(j)&& aller_haut(j)) && keep && parties[id].grille[j-7]!=undefined){
            j=j-7;
            if(parties[id].grille[j].couleur==jo.couleur){
                cpt++;
                if(cpt==4){
                    return true;
                }
            }else{
                keep=false;
            }
        }
        j=i;
        keep=true;
        while((aller_droite(j)&& aller_bas(j)) && keep && parties[id].grille[j+7]!=undefined){
            j=j+7;
            if(parties[id].grille[j].couleur==jo.couleur){
                cpt++;
                if(cpt==4){
                    return true;
                }
            }else{
                keep=false;
            }
        }
        //TEST VICTOIRE DIAGONAL 2 (HAUT DROITE BAS GAUCHE)
        cpt=1;
        j=i;
        keep=true;
        while((aller_droite(j)&& aller_haut(j)) && keep && parties[id].grille[j-5]!=undefined){
            j=j-5;
            if(parties[id].grille[j].couleur==jo.couleur){
                cpt++;
                if(cpt==4){
                    return true;
                }
            }else{
                keep=false;
            }
        }
        j=i;
        keep=true;
        while((aller_gauche(j)&& aller_bas(j)) && keep && parties[id].grille[j+5]!=undefined){
            j=j+5;
            if(parties[id].grille[j].couleur==jo.couleur){
                cpt++;
                if(cpt==4){
                    return true;
                }
            }else{
                keep=false;
            }
        }

    }
    return false;
}
/**
 * Fonction qui affiche une carte en mode console
 */
function afficher_carte_console(car){
    if(car==undefined){
        return "  ";
    }
    let ch="";
    switch(car.couleur){
        case "red":
            ch+="R";
            break;
        case "blue":
            ch+="B";
            break;
        case "green":
            ch+="G";
            break;
        case "yellow":
            ch+="Y";
            break;
        
    }
    return ch+=car.valeur;
}
/**
 * Fonction qui affiche une grille en mode console
 */
function afficher_grille_console(id){
    let ch="";
    for(let a=0;a<=35;a++){
        if(a%6==0){
            ch+="|";
        }
        ch+=afficher_carte_console(parties[id].grille[a]);
        if(a%6==5){
            ch+="|\n";
        }else{
            ch+="|";
        }
    }
}
 /**
  *  Enregistre un défi ou résoud une bataille entre deux joueurs.
  *  @param  string  joueur1     le pseudo du premier joueur
  *  @param  string  joueur2     le pseudo du second joueur
  *  @param  string  choixJ1     le choix du joueur 1
  */
function joinPunto(self) {
    if (! defis[self]) {
        return { status: -1, message: `Le joueur ${self} n'existe pas.` };   
    }
    return { status: 0, message: `Le joueur ${self} rejoint la partie.` }; 
}
function ajouter(pseudo) {
    if (defis[pseudo]) {
        return false;   
    }
    defis[pseudo] = {};
    scores[pseudo] = 0;
    return true;
}
/**
 *  Supprime l'utilisateur passé en paramètre et efface ses scores et ses défis (lancés ou en attente). 
 */
function supprimer(pseudo) {
    // suppression des lanceurs de défis
    delete defis[pseudo];
    // suppression des scores
    delete scores[pseudo];
    // suppression 
    for (let p in defis) {
        delete defis[p][pseudo];   
    }
}
/**
 *  Renvoie un chaine JSON 
 */
function scoresJSON() {
    return JSON.stringify(scores);   
}
/**
 *  Compare les mains, détermine le vainqueur, le perdant, supprime le défi (chez joueur1), met à jour les scores
 *  et renvoie un objet informant du résultat. 
 *  @return 
 */
function bataille(joueur1, choix1, joueur2, choix2) {
    // cas d'un match nul
    if (choix1 == choix2) {
        var matchNul = `:${choix1}: vs. :${choix2}: - égalité`;
        // supprime le défi chez joueur1
        delete defis[joueur1][joueur2];
        return { vainqueur: null, perdant: null, message: matchNul };
    }
    
    // compare les mains et détermine le vainqueur et le perdant
    var vainqueur = (beats[choix1][choix2]) ? joueur1 : joueur2;
    var perdant = (vainqueur == joueur1) ? joueur2 : joueur1;
    // notifie le vainqueur et le perdant
    var resume = (beats[choix1][choix2]) 
                ? `:${choix1}: ${beats[choix1][choix2]} :${choix2}:` 
                : `:${choix2}: ${beats[choix2][choix1]} :${choix1}:`;
    // supprime le défi chez joueur1
    delete defis[joueur1][joueur2];
    // met à jour le score 
    scores[vainqueur]++;
    return { vainqueur: vainqueur, perdant: perdant, message: resume };
}
 // Définition des fonctions exportées
 module.exports = { ajouter, supprimer, joinPunto, scoresJSON,creerPartie,creerJoueur,lancer_partie,tirage_carte_debut,tirage_carte,tour_initial,tour_normal,a_gagne,getPartie,afficher_grille_console};
 