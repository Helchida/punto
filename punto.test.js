const assert = require('assert').strict;

const { afficher_grille_console } = require('./punto.js');
const punto = require('./punto.js');

describe("Creation partie et joueurs", function() {

    it("doit pouvoir creer une partie", function() {
        let id=punto.creerPartie();
        assert.strictEqual(id, 3); 
    });
    
    it("doit pouvoir creer une seconde partie", function() {
        let id=punto.creerPartie();
        let id2=punto.creerPartie();
        assert.strictEqual(id2, 5); 
    });

    it("doit pouvoir ajouter un joueur", function() {
        let id=punto.creerPartie();
        let ok=punto.creerJoueur("Momo",id);
        assert.strictEqual(ok, true); 
    });

    it ("doit rejeter un joueur present", function() {
        let id=punto.creerPartie();
        punto.creerJoueur("Momo",id);
        assert.strictEqual(punto.creerJoueur("Momo",id), false); 
    });

    it("doit rejeter 5ieme joueur", function() {
        let id=punto.creerPartie();
        assert.strictEqual(punto.creerJoueur("Momo",id), true); 
        assert.strictEqual(punto.creerJoueur("Hugo",id), true); 
        assert.strictEqual(punto.creerJoueur("Thomas",id), true); 
        assert.strictEqual(punto.creerJoueur("Max",id), true); 
        assert.strictEqual(punto.creerJoueur("Ryan",id), false); 
    });

    it ("doit rejeter un joueur apres partie lance", function() {
        let id=punto.creerPartie();
        assert.strictEqual(punto.creerJoueur("Momo",id), true); 
        assert.strictEqual(punto.creerJoueur("Thomas",id), true);
        assert.strictEqual(punto.lancer_partie(id,"Momo"),true);
        assert.strictEqual(punto.creerJoueur("Ryan",id), false); 
    });
    
    it("doit tirer les cartes pour tout les joueurs d'une partie",function(){
        let id=punto.creerPartie();
        assert.strictEqual(punto.creerJoueur("Momo",id), true); 
        assert.strictEqual(punto.creerJoueur("Hugo",id), true); 
        assert.strictEqual(punto.creerJoueur("Thomas",id), true); 
        assert.strictEqual(punto.creerJoueur("Max",id), true);
        assert.strictEqual(punto.lancer_partie(id,"Momo"),true); 
        assert.strictEqual(punto.tirage_carte_debut(id), true); 
    });

    it("doit réaliser le tour initial",function(){
        let id=punto.creerPartie();
        assert.strictEqual(punto.creerJoueur("Momo",id), true); 
        assert.strictEqual(punto.creerJoueur("Hugo",id), true); 
        assert.strictEqual(punto.creerJoueur("Thomas",id), true); 
        assert.strictEqual(punto.creerJoueur("Max",id), true);
        assert.strictEqual(punto.lancer_partie(id,"Momo"),true); 
        assert.strictEqual(punto.tirage_carte_debut(id), true); 
        assert.strictEqual(punto.tour_initial(id), true);
        console.log("Case 14 :"+punto.getPartie(id).grille[14].valeur);
    });

    it("peut poser une carte à coté",function(){
        let id=punto.creerPartie();
        assert.strictEqual(punto.creerJoueur("Momo",id), true); 
        assert.strictEqual(punto.creerJoueur("Hugo",id), true); 
        assert.strictEqual(punto.creerJoueur("Thomas",id), true); 
        assert.strictEqual(punto.creerJoueur("Max",id), true);
        assert.strictEqual(punto.lancer_partie(id,"Momo"),true); 
        assert.strictEqual(punto.tirage_carte_debut(id), true); 
        assert.strictEqual(punto.tour_initial(id), true);
        let partie=punto.getPartie(id);
        assert.strictEqual(punto.tour_normal(id,13,partie.joueurs[1].pseudo),true);
        afficher_grille_console(id);
    });

    it("ne peut poser une carte sans carte à coté",function(){
        let id=punto.creerPartie();
        assert.strictEqual(punto.creerJoueur("Momo",id), true); 
        assert.strictEqual(punto.creerJoueur("Hugo",id), true); 
        assert.strictEqual(punto.creerJoueur("Thomas",id), true); 
        assert.strictEqual(punto.creerJoueur("Max",id), true);
        assert.strictEqual(punto.lancer_partie(id,"Momo"),true); 
        assert.strictEqual(punto.tirage_carte_debut(id), true); 
        assert.strictEqual(punto.tour_initial(id), true);
        let partie=punto.getPartie(id);
        assert.strictEqual(punto.tour_normal(id,2,partie.joueurs[1].pseudo),false);
        afficher_grille_console(id);
    });

    it("peut poser une carte sur une autre si carte sup",function(){
        let id=punto.creerPartie();
        let partie=punto.getPartie(id);
        assert.strictEqual(punto.creerJoueur("Momo",id), true); 
        assert.strictEqual(punto.creerJoueur("Hugo",id), true); 
        assert.strictEqual(punto.creerJoueur("Thomas",id), true); 
        assert.strictEqual(punto.creerJoueur("Max",id), true);
        assert.strictEqual(punto.lancer_partie(id,"Momo"),true); 
        assert.strictEqual(punto.tirage_carte_debut(id), true); 
        partie.joueurs[0].cartes[partie.joueurs[0].indice_carte].valeur=5;
        assert.strictEqual(punto.tour_initial(id), true);
        afficher_grille_console(id);
        partie.joueurs[1].cartes[partie.joueurs[1].indice_carte].valeur=9;
        assert.strictEqual(punto.tour_normal(id,14,partie.joueurs[1].pseudo),true);
        afficher_grille_console(id);
    });

    it("ne peut poser une carte sur une autre si carte inf",function(){
        let id=punto.creerPartie();
        let partie=punto.getPartie(id);
        assert.strictEqual(punto.creerJoueur("Momo",id), true); 
        assert.strictEqual(punto.creerJoueur("Hugo",id), true); 
        assert.strictEqual(punto.creerJoueur("Thomas",id), true); 
        assert.strictEqual(punto.creerJoueur("Max",id), true);
        assert.strictEqual(punto.lancer_partie(id,"Momo"),true); 
        assert.strictEqual(punto.tirage_carte_debut(id), true); 
        partie.joueurs[0].cartes[partie.joueurs[0].indice_carte].valeur=5;
        assert.strictEqual(punto.tour_initial(id), true);
        afficher_grille_console(id);
        partie.joueurs[1].cartes[partie.joueurs[1].indice_carte].valeur=2;
        assert.strictEqual(punto.tour_normal(id,14,partie.joueurs[1].pseudo),false);
        afficher_grille_console(id);
    });

    it("ne peut poser une carte sur une autre si carte egale",function(){
        let id=punto.creerPartie();
        let partie=punto.getPartie(id);
        assert.strictEqual(punto.creerJoueur("Momo",id), true); 
        assert.strictEqual(punto.creerJoueur("Hugo",id), true); 
        assert.strictEqual(punto.creerJoueur("Thomas",id), true); 
        assert.strictEqual(punto.creerJoueur("Max",id), true);
        assert.strictEqual(punto.lancer_partie(id,"Momo"),true); 
        assert.strictEqual(punto.tirage_carte_debut(id), true); 
        partie.joueurs[0].cartes[partie.joueurs[0].indice_carte].valeur=5;
        assert.strictEqual(punto.tour_initial(id), true);
        afficher_grille_console(id);
        partie.joueurs[1].cartes[partie.joueurs[1].indice_carte].valeur=5;
        assert.strictEqual(punto.tour_normal(id,14,partie.joueurs[1].pseudo),false);
        afficher_grille_console(id);
    });

    it("obtient une victoire verticale",function(){
        let id=punto.creerPartie();
        let partie=punto.getPartie(id);
        assert.strictEqual(punto.creerJoueur("Momo",id), true); 
        assert.strictEqual(punto.creerJoueur("Hugo",id), true); 
        assert.strictEqual(punto.lancer_partie(id,"Momo"),true); 
        partie.grille[8]=partie.joueurs[0].cartes[2];
        partie.grille[2]=partie.joueurs[0].cartes[3];
        partie.grille[14]=partie.joueurs[0].cartes[4];
        partie.grille[20]=partie.joueurs[0].cartes[5];
        afficher_grille_console(id);
        assert.strictEqual(punto.a_gagne(id,20,partie.joueurs[0]),true);
    });

    it("obtient une victoire horizontale",function(){
        let id=punto.creerPartie();
        let partie=punto.getPartie(id);
        assert.strictEqual(punto.creerJoueur("Momo",id), true); 
        assert.strictEqual(punto.creerJoueur("Hugo",id), true); 
        assert.strictEqual(punto.lancer_partie(id,"Momo"),true); 
        partie.grille[15]=partie.joueurs[0].cartes[2];
        partie.grille[13]=partie.joueurs[0].cartes[3];
        partie.grille[14]=partie.joueurs[0].cartes[4];
        partie.grille[16]=partie.joueurs[0].cartes[5];
        afficher_grille_console(id);
        assert.strictEqual(punto.a_gagne(id,16,partie.joueurs[0]),true);
    });

    it("obtient une victoire diagonale haut gauche",function(){
        let id=punto.creerPartie();
        let partie=punto.getPartie(id);
        assert.strictEqual(punto.creerJoueur("Momo",id), true); 
        assert.strictEqual(punto.creerJoueur("Hugo",id), true); 
        assert.strictEqual(punto.lancer_partie(id,"Momo"),true); 
        partie.grille[21]=partie.joueurs[0].cartes[2];
        partie.grille[7]=partie.joueurs[0].cartes[3];
        partie.grille[14]=partie.joueurs[0].cartes[4];
        partie.grille[28]=partie.joueurs[0].cartes[5];
        afficher_grille_console(id);
        assert.strictEqual(punto.a_gagne(id,28,partie.joueurs[0]),true);
    });

    it("obtient une victoire diagonale haut droite",function(){
        let id=punto.creerPartie();
        let partie=punto.getPartie(id);
        assert.strictEqual(punto.creerJoueur("Momo",id), true); 
        assert.strictEqual(punto.creerJoueur("Hugo",id), true); 
        assert.strictEqual(punto.lancer_partie(id,"Momo"),true); 
        partie.grille[9]=partie.joueurs[0].cartes[2];
        partie.grille[19]=partie.joueurs[0].cartes[3];
        partie.grille[14]=partie.joueurs[0].cartes[4];
        partie.grille[24]=partie.joueurs[0].cartes[5];
        afficher_grille_console(id);
        assert.strictEqual(punto.a_gagne(id,24,partie.joueurs[0]),true);
    });

    

});